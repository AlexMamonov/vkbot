let express = require('express');
let app = express();
let mongojs= require('mongojs');
let db=mongojs('messages',['messages']);
let bodyParser= require('body-parser');

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

app.get('/messages', function(req, res){
    console.log("i recieve a get request")

    db.messagelist.find(function(err, docs){
    console.log(docs);
    res.json(docs);

    });

});

app.post('/messages', function (req, res){
    console.log(req.body);
    db.messagelist.insert(req.body, function(err, doc){
        res.json(doc);
    })
});
app.delete('/messages/:id', function(req, res){
    let id = req.params.id;
    console.log(id);
    db.messagelist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.get('/messages/:id', function(req, res){
    let id = req.params.id;
    console.log(id);
    db.messagelist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.put('/messages/:id', function(req, res){
    let id =req.params.id;
    console.log(req.body.name);
    db.messagelist.findAndModify({query:{_id: mongojs.ObjectId(id)},
        update:{$set: {name:req.body.name, message:req.body.message}},
        new: true}, function(err, doc){
res.json(doc);
        
    });

    });


app.listen(3001);
console.log("server running at port 3001")